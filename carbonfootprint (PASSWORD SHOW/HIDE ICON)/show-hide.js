import React, { Component } from 'react';
import { KeyboardAvoidingView, SafeAreaView, View, Alert } from 'react-native'
import { Form, Item, Label, Input, Icon, Button, Text } from 'native-base'

export default class App extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      textEntry: true,
      hidden: true,
    }
  }
  showPwd = () => {    
    this.setState({textEntry: !this.state.textEntry})
  }

  submitBtn= () => {
    
  }
  render() {
    return (
      <KeyboardAvoidingView enabled style={{flex: 1}}>
        <SafeAreaView style={{flex: 1}}>
          <View style={{flex: 1}}>
            <Form style={{flex: 1}}>
              <Item floatingLabel>
                <Label>Email</Label>
                <Input keyboardType="email-address" autoCapitalize="none" autoCorrect={false} autoCompleteType="email"/>
              </Item>
              <Item floatingLabel>
                <Label>Password</Label>
                <Input secureTextEntry={this.state.textEntry} keyboardType="default" autoCapitalize="words" autoCorrect={false}/>
                <Button primary>
                </Button>
                <Icon name='glasses' style={{fontSize: 30, color: 'grey'}} onPress={this.showPwd}/>
              </Item>
              <Button primary full rounded style={{margin: 20}} onPress={this.submitBtn}>
                <Text style={{color: '#FFFFFF'}}>Submit</Text>
              </Button>
            </Form>
          </View>
        </SafeAreaView>
      </KeyboardAvoidingView>
       
    );
  }
}
